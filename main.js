document.getElementById('task-form').addEventListener('submit', function(event) {
  event.preventDefault();

  const taskInput = document.getElementById('task-input');
  const taskList = document.getElementById('task-list');

  const newTask = document.createElement('li');
  newTask.textContent = taskInput.value;
  newTask.classList.add('border', 'border-gray-300', 'bg-white', 'px-5', 'py-2', 'mt-2', 'rounded-lg', 'text-sm', 'flex', 'justify-between', 'items-center');

  const deleteButton = document.createElement('button');
  deleteButton.innerHTML = '<i class="fa fa-trash-alt text-red-500"></i>';
  deleteButton.addEventListener('click', function() {
    taskList.removeChild(newTask);
  });

  newTask.appendChild(deleteButton);
  taskList.appendChild(newTask);

  taskInput.value = '';
});
